export interface IAsyncData<T> {
    loading: boolean;
    data: T[] | T | null;
    errors?: string[];
}

export interface IDictionaryItem {
    label: string;
    value: number | string;
}
