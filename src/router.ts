import Vue from 'vue';
import Router from 'vue-router';

// Containers
const MainLayout = () => import(/* webpackChunkName: "mainLayout" */ '@/containers/MainLayout.vue');
const Page404 = () => import(/* webpackChunkName: "page404" */ '@/containers/Page404.vue');

// Views
const Operations = () => import(/* webpackChunkName: "operations" */ '@/views/operations/OperationList.vue');

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'root',
      component: MainLayout,
        children: [
            {
                path: '/',
                redirect: '/operations',
                name: 'home',
            },
            {
                path: 'operations',
                component: Operations,
            },
            {
                path: '*',
                name: '404',
                component: Page404,
            },
        ],
    },
  ],
});
