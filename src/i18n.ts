import Vue from 'vue';
import VueI18n from 'vue-i18n';
import localeI18n from '@/locales/intterra-ru-RU.json';

Vue.use(VueI18n);

// Ready translated locale messages
const messages = {
    ru: {
        ...localeI18n,
    },
};

const dateTimeFormats = {
    ru: {
        short: {
            year: 'numeric', month: 'short', day: 'numeric',
        },
        long: {
            year: 'numeric', month: 'numeric', day: 'numeric',
            hour: '2-digit', minute: 'numeric', second: 'numeric',
        },
    },
};

// Create VueI18n instance with options
export default new VueI18n({
    locale: 'ru', // set locale
    dateTimeFormats,
    messages, // set locale messages
});
