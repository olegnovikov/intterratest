import {ActionContext, GetterTree, MutationTree} from 'vuex';
import FieldService from '@/services/FieldService';
import {IOperationState} from '@/store/modules/operations/types';
import Operation from '@/models/Operation';

const fieldService = new FieldService();

const moduleState: IOperationState = {
    list: {
        loading: false,
        data: [],
        errors: [],
    },
    selected: {
        loading: false,
        data: null,
        errors: [],
    },
};

const mutations: MutationTree<IOperationState> = {
    listLoading: (state) => {
        state.list.loading = true;
        state.list.data = [];
    },
    listLoaded: (state, payload) => {
        state.list.loading = false;
        state.list.data = payload;
    },

    selectedLoading: (state) => {
        state.selected.loading = true;
        state.selected.data = null;
    },
    selectedLoaded: (state, payload) => {
        state.selected.loading = false;
        state.selected.data = payload;
    },
};

const actions = {
    getOperations: async ({commit}: ActionContext<IOperationState, null>) => {
        try {
            commit('listLoading');
            const operations = await fieldService.getOperations();
            commit('listLoaded', operations);
        } catch (e) {
            alert('Ошибка загрузки списка операций');
        }
    },
    getOperation: async ({commit}: ActionContext<IOperationState, null>, operationId: string) => {
        try {
            commit('selectedLoading');
            const operation = await fieldService.getOperation(operationId);
            commit('selectedLoaded', operation);
        } catch (e) {
            alert('Ошибка загрузки операции');
        }
    },
    saveOperation: async ({commit, dispatch}: ActionContext<IOperationState, null>, operation: Operation) => {
        try {
            await fieldService.saveOperation(operation);
            dispatch('getOperations');
        } catch (e) {
            alert('Ошибка сохранения операции');
        }
    },
};

const getters: GetterTree<IOperationState, null> = {
    filteredOperations: (state: any) => (complete: boolean) => complete ?
        state.list :
        {...state.list, data: state.list.data.filter((operation: Operation) => operation.assessment === null)},
};

export default {
    namespaced: true,
    state: moduleState,
    getters,
    mutations,
    actions,
};
