import {namespace} from 'vuex-class';
import {IAsyncData} from '@/models/Base';
import {IOperation} from '@/models/Operation';

export interface IOperationState {
    list: IAsyncData<IOperation[]>;
    selected: IAsyncData<IOperation>;
}

export const operationModule = namespace('operations');
