import Vue from 'vue';
import Vuex from 'vuex';

import operations from './modules/operations/index';

Vue.use(Vuex);

// root state object
export default new Vuex.Store<null> ({
    strict: process.env.NODE_ENV !== 'production',
    modules: {
        operations,
    },
});
