declare module 'element-ui';
declare module 'element-ui/lib/locale';
declare module 'element-ui/lib/locale/lang/ru-RU.js';

declare module '*.json' {
    const value: any;
    export default value;
}
