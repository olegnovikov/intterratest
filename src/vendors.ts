import Vue from 'vue';

// UI library
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU.js';

Vue.use(ElementUI, { locale });

// Icon font
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSeedling, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faSeedling);
library.add(faPlusCircle);

Vue.component('font-awesome-icon', FontAwesomeIcon);

